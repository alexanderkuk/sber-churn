#!/usr/bin/env python
#!encoding: utf8

import sys
import re
import os.path
import random
from datetime import datetime
from itertools import islice
from collections import namedtuple, Counter, defaultdict

import seaborn as sns
import numpy as np
import pandas as pd
import networkx as nx
from matplotlib import pyplot as plt
from sklearn.metrics import roc_curve, auc
from sklearn.preprocessing import OneHotEncoder, StandardScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.pipeline import Pipeline
from sklearn.grid_search import GridSearchCV


DATA_DIR = 'data'
RAW_TRAIN = os.path.join(DATA_DIR, 'train.csv')
TRAIN_SAMPLE = os.path.join(DATA_DIR, 'train_sample.csv')
TRAIN_SIZE = 25259805
TARGET_TRAIN = os.path.join(DATA_DIR, 'target_train.csv')
TARGET_TEST = os.path.join(DATA_DIR, 'target_test.csv')
NOVEMBER = datetime(2014, 11, 1, 0, 0)
DECEMBER = datetime(2014, 12, 1, 0, 0)
JANUARY = datetime(2015, 1, 1, 0, 0)
FEBRUARY = datetime(2015, 2, 1, 0, 0)
TRANSACTIONS_GRAPH = 'transactions.gexf'
NULL_HASH = 'K+iMpCQsduglOsYkdIUQZQMtaDM='
NULL_ID = NULL_HASH * 2

TrainRecord = namedtuple('TrainRecord', 'id1, id2, features')
TargetRecord = namedtuple('TargetRecord', 'month, id, churn')

CHURN = 1
STAY = 0
UNKNOWN_CHURN = 2

REGIONS = [
    u'москва',
    u'санкт-петербург',
]
OTHER_REGION = len(REGIONS)
UNKOWN_REGION = OTHER_REGION + 1
REGIONS_MAPPING = defaultdict(lambda: OTHER_REGION)
for index, value in enumerate(REGIONS):
    REGIONS_MAPPING[value] = index
REGIONS_MAPPING['null'] = UNKOWN_REGION

ADDRESS_TYPES = [
    'Почтовый',
    'Юридический',
    'Фактический',
    'Регистрация'
]
OTHER_ADDRESS_TYPE = len(ADDRESS_TYPES)
UNKOWN_ADDRESS_TYPE = OTHER_ADDRESS_TYPE + 1
ADDRESS_TYPES_MAPPING = defaultdict(lambda: OTHER_ADDRESS_TYPE)
for index, type in enumerate(ADDRESS_TYPES):
    ADDRESS_TYPES_MAPPING[type] = index

FEATURE_8 = [
    '8Fr5xcRCoQZZcqEbpcAqTs0gk6Q=',
    '8NHZwcvtbn+LCCz43s7pGOY4UBs=',
    'EDFQTLPGQ9VARpCGIetY6kFTC5o='
]
OTHER_FEATURE_8 = len(FEATURE_8)
UNKOWN_FEATURE_8 = OTHER_FEATURE_8 + 1
FEATURE_8_MAPPING = defaultdict(lambda: OTHER_FEATURE_8)
for index, value in enumerate(FEATURE_8):
    FEATURE_8_MAPPING[value] = index
FEATURE_8_MAPPING['K+iMpCQsduglOsYkdIUQZQMtaDM='] = UNKOWN_FEATURE_8

FEATURE_9 = [
    '+0Z6k3cF2OYlodTc/ry7ENdHj/4=',
    '0Dw2prbHHA/LjiH1MNv+EwbrEIo=',
    '1X+UBlG07t36gjSIIdPmmPAqC6M=',
    '2wNaK88zG9Yx/u5F2lFvNBHBgq4=',
    '3Ji3G+ARmzO1yVBTSXEWqZ/RpUI=',
    '3oBULTFHqc+mMm+EwPEUZWT99e0=',
    'Antbbaf85R6dGC6FKWOZhUWY0Ps=',
    'Qx2Xcbw18JVCe8zA9Mg/hBGOZH8=',
    'STEpHhIi9YenZHm1vdcY+taAg9w=',
    'pwlhB6ysDVW2zQM0uC0q2kWUGRE='
]
OTHER_FEATURE_9 = len(FEATURE_9)
UNKOWN_FEATURE_9 = OTHER_FEATURE_9 + 1
FEATURE_9_MAPPING = defaultdict(lambda: OTHER_FEATURE_9)
for index, value in enumerate(FEATURE_9):
    FEATURE_9_MAPPING[value] = index

FEATURE_18 = [
    'qyu3UzMRa6YpWn8gCAxEIHyDDXE=',
    '1VGXQs9SOYOM013xFyyZws/yG4E=',
    'zEk6wnZ3jSUbZmJzwPUzVAsPYRc=',
    'oTXLCX+1oY7j5bQRHQeBnlv6pv0=',
    '7cm+7jG90gqPL+rJ+XKTY6DE5Ac=',
    'yqFVMcNVHZnmcvXBFzbVgt0mE2Q=',
    'jQGVk70yBuv4Axvcut2zRj+Fe9w=',
    'yIyaqTSAZrtzfD8Bka37/9dZrY4=',
]
OTHER_FEATURE_18 = len(FEATURE_18)
UNKOWN_FEATURE_18 = OTHER_FEATURE_18 + 1
FEATURE_18_MAPPING = defaultdict(lambda: OTHER_FEATURE_18)
for index, value in enumerate(FEATURE_18):
    FEATURE_18_MAPPING[value] = index
FEATURE_18_MAPPING['K+iMpCQsduglOsYkdIUQZQMtaDM='] = UNKOWN_FEATURE_18

FEATURE_19 = [
    'ByHiij4WTahzuNNLPcqJdKpoQ9I=',
    'mIK9A1hDE9uL+Q6n/lTSZt5Fkvg=',
    'NUAn3JUSJ9E1W/aQf2YPd6PsPco=',
    'mWaUXUmxfs1Do/4eYenTEKHhuO0=',
    'lGKaUapcszPSt6UO77mm3R9jC1c=',
    'nL54gVoDrtjQ5xGRzi1ntv91K5Y=',
    'XZSXxPYq/RvkEEc2PeMiuIu7Vns=',
    '00ZjuiHDokQNBIMhqwqldGG8hkk='
]
OTHER_FEATURE_19 = len(FEATURE_19)
UNKOWN_FEATURE_19 = OTHER_FEATURE_19 + 1
FEATURE_19_MAPPING = defaultdict(lambda: OTHER_FEATURE_19)
for index, value in enumerate(FEATURE_19):
    FEATURE_19_MAPPING[value] = index
FEATURE_19_MAPPING['K+iMpCQsduglOsYkdIUQZQMtaDM='] = UNKOWN_FEATURE_19

FEATURE_21 = [
    'ckhHdedZD2GVknDR0U7cb8Ud9WM=',
    'hv1tI60U6aQVjPWhvsbBiCtFsp8=',
    '4LLYHIC5R5mu7xJvmQ9IzbVQW7w=',
    'pYLrAbmbhUVW5gMmGXg2xvUNHqY=',
    'opX1JeOvwb7qEv1TiFxXQrvKpNQ=',
    'l6h7Rw/p7V/1H/m4VD6TfmAW1Iw=',
    'Y0HgA1ILx3nJltJJAwWll/9FqK4=',
    'ilMHcoS+W3l3cnDvm1NfQuedU+E='
]
OTHER_FEATURE_21 = len(FEATURE_21)
UNKOWN_FEATURE_21 = OTHER_FEATURE_21 + 1
FEATURE_21_MAPPING = defaultdict(lambda: OTHER_FEATURE_21)
for index, value in enumerate(FEATURE_21):
    FEATURE_21_MAPPING[value] = index
FEATURE_21_MAPPING['K+iMpCQsduglOsYkdIUQZQMtaDM='] = UNKOWN_FEATURE_21

FEATURE_22 = [
    'nSW16PgRSzY3S3D4i6YcNyMBubY=',
    'o+YcaQExf8aGYMWInGv960ffShU=',
    'v4Nn6tTHtoYfjbzuAFy9dkAxH30='
]
OTHER_FEATURE_22 = len(FEATURE_22)
UNKOWN_FEATURE_22 = OTHER_FEATURE_22 + 1
FEATURE_22_MAPPING = defaultdict(lambda: OTHER_FEATURE_22)
for index, value in enumerate(FEATURE_22):
    FEATURE_22_MAPPING[value] = index
FEATURE_22_MAPPING['K+iMpCQsduglOsYkdIUQZQMtaDM='] = UNKOWN_FEATURE_22

FEATURE_43 = [
    'Комиссия РКО',
    'Оплата документа картотеки',
    'Выделен НДС',
    'Не выделять НДС',
    'Переоценка счета.',
    'Ожидание ДПП',
    'Дооценка',
    'За инкассацию',
]
OTHER_FEATURE_43 = len(FEATURE_43)
UNKOWN_FEATURE_43 = OTHER_FEATURE_43 + 1
FEATURE_43_MAPPING = defaultdict(lambda: OTHER_FEATURE_43)
for index, value in enumerate(FEATURE_43):
    FEATURE_43_MAPPING[value] = index
FEATURE_43_MAPPING['null'] = UNKOWN_FEATURE_43

FEATURE_45 = [
    'Гашение учтенных процентов',
    'Гашение кредита',
    'Зачисление депозита',
    'Гашение просроченной задолженности',
    'Гашение пени по кредиту',
    'Гашение пени по процентам',
    'Гашение задолженности по процентам',
    'Гашение платы за открытый лимит'
]
OTHER_FEATURE_45 = len(FEATURE_45)
UNKOWN_FEATURE_45 = OTHER_FEATURE_45 + 1
FEATURE_45_MAPPING = defaultdict(lambda: OTHER_FEATURE_45)
for index, value in enumerate(FEATURE_45):
    FEATURE_45_MAPPING[value] = index
FEATURE_45_MAPPING['null'] = UNKOWN_FEATURE_45

FEATURE_47 = [
    'Возврат депозита',
    'Выдача кредита',
    'Постановка договора обеспечения на внебаланс'
]
OTHER_FEATURE_47 = len(FEATURE_47)
UNKOWN_FEATURE_47 = OTHER_FEATURE_47 + 1
FEATURE_47_MAPPING = defaultdict(lambda: OTHER_FEATURE_47)
for index, value in enumerate(FEATURE_47):
    FEATURE_47_MAPPING[value] = index
FEATURE_47_MAPPING['null'] = UNKOWN_FEATURE_47

CURRENCIES = [
    'RUR',
    'USD',
    'EUR'
]
OTHER_CURRENCY = len(CURRENCIES)
UNKNOWN_CURRENCY = OTHER_CURRENCY + 1
CURRENCIES_MAPPING = defaultdict(lambda: OTHER_CURRENCY)
for index, currency in enumerate(CURRENCIES):
    CURRENCIES_MAPPING[currency] = index

TYPES = [
    'PROV',
    'PAY',
    'TO_RETURN',
    'FORM',
    'ISNULL',
    'DELETED',
    'TO_KART',
    'VOZV',
    'TO_RKO'
]
OTHER_TYPE = len(TYPES)
TYPES_MAPPING = defaultdict(lambda: OTHER_TYPE)
for index, value in enumerate(TYPES):
    TYPES_MAPPING[value] = index

FEATURES_NAMES = ['before_previous_month_churn', 'previous_month_churn',
                  'in_transactions', 'out_transactions',
                  'in_months', 'out_months',
                  'months_to_first_in_transaction', 'months_to_last_in_transaction',
                  'months_to_first_out_transaction', 'months_to_last_out_transaction',
                  'address_type',
                  'feature_8',
                  'feature_9',
                  'region',
                  'currency',

                  'feature_18_1', 'feature_18_2', 'feature_18_3', 'feature_18_4',
                  'feature_18_5', 'feature_18_6', 'feature_18_7', 'feature_18_8',
                  'feature_18_9', 'feature_18_10',

                  'feature_19_1', 'feature_19_2', 'feature_19_3', 'feature_19_4',
                  'feature_19_5', 'feature_19_6', 'feature_19_7', 'feature_19_8',
                  'feature_19_9', 'feature_19_10',

                  'feature_21_1', 'feature_21_2', 'feature_21_3', 'feature_21_4',
                  'feature_21_5', 'feature_21_6', 'feature_21_7', 'feature_21_8',
                  'feature_21_9', 'feature_21_10',

                  'feature_22_1', 'feature_22_2', 'feature_22_3',
                  'feature_22_4', 'feature_22_5',

                  'feature_43_1', 'feature_43_2', 'feature_43_3', 'feature_43_4',
                  'feature_43_5', 'feature_43_6', 'feature_43_7', 'feature_43_8',
                  'feature_43_9', 'feature_43_10',

                  'feature_45_1', 'feature_45_2', 'feature_45_3', 'feature_45_4',
                  'feature_45_5', 'feature_45_6', 'feature_45_7', 'feature_45_8',
                  'feature_45_9', 'feature_45_10',

                  'feature_47_1', 'feature_47_2', 'feature_47_3',
                  'feature_47_4', 'feature_47_5',

                  'types_1', 'types_2', 'types_3', 'types_4', 'types_5',
                  'types_6', 'types_7', 'types_8', 'types_9', 'types_10',

                  'in_amount', 'out_amount']
CATEGORICAL_FEATURES = {
    'before_previous_month_churn': 3,
    'previous_month_churn': 3,
    'address_type': 6,
    'currency': 5,
    'region': 4,
    'feature_8': 5,
    'feature_9': 12
}
Features = namedtuple('Features', FEATURES_NAMES)


def log_progress(stream, every=1000, total=None):
    if total:
        every = total / 200     # every 0.5%
    for index, record in enumerate(stream):
        if index % every == 0:
            if total:
                progress = float(index) / total
                progress = '{0:0.2f}%'.format(progress * 100)
            else:
                progress = index
            print >>sys.stderr, progress,
        yield record


def read_train(path):
    with open(path) as file:
        for line in file:
            (Y1, Y2, Y3, Y4, Y5, Y6, Y7, Y8, Y9, Y10, ID_1_1, ID_2_1,
             Y11, Y12, Y13, Y14, Y15, Y16, Y17, Y18, Y19, Y20, Y21,
             Y22, Y23, Y24, Y25, Y26, Y27, Y28, Y29, ID_1_2, ID_2_2,
             Y30, Y31, Y32, Y33, Y34, Y35, Y36, Y37, Y38, Y39, Y40,
             Y41, Y42, Y43, Y44, Y45, Y46, Y47, Y48, Y49, Y50, Y51,
             Y52, Y53, Y54, Y55, Y56, Y57, Y58) = line.rstrip('\n').split('\t')
            yield TrainRecord(
                ID_1_1 + ID_2_1,
                ID_1_2 + ID_2_2,
                (Y1, Y2, Y3, Y4, Y5,
                 Y6, Y7, Y8, Y9, Y10, Y11, Y12, Y13, Y14, Y15, Y16,
                 Y17, Y18, Y19, Y20, Y21, Y22, Y23, Y24, Y25, Y26,
                 Y27, Y28, Y29, Y30, Y31, Y32, Y33, Y34, Y35, Y36,
                 Y37, Y38, Y39, Y40, Y41, Y42, Y43, Y44, Y45, Y46,
                 Y47, Y48, Y49, Y50, Y51, Y52, Y53, Y54, Y55, Y56,
                 Y57, Y58)
            )


def split_id(id):
    return re.match('([^=]+=)([^=]+=)', id).groups()


def write_train(data, path):
    with open(path, 'w') as file:
         for record in data:
             id1, id2, features = record
             ID_1_1, ID_2_1 = split_id(id1)
             ID_1_2, ID_2_2 = split_id(id2)
             (Y1, Y2, Y3, Y4, Y5,
              Y6, Y7, Y8, Y9, Y10, Y11, Y12, Y13, Y14, Y15, Y16,
              Y17, Y18, Y19, Y20, Y21, Y22, Y23, Y24, Y25, Y26,
              Y27, Y28, Y29, Y30, Y31, Y32, Y33, Y34, Y35, Y36,
              Y37, Y38, Y39, Y40, Y41, Y42, Y43, Y44, Y45, Y46,
              Y47, Y48, Y49, Y50, Y51, Y52, Y53, Y54, Y55, Y56,
              Y57, Y58) = features
             file.write(
                 '\t'.join(
                     (Y1, Y2, Y3, Y4, Y5, Y6, Y7, Y8, Y9, Y10, ID_1_1, ID_2_1,
                      Y11, Y12, Y13, Y14, Y15, Y16, Y17, Y18, Y19, Y20, Y21,
                      Y22, Y23, Y24, Y25, Y26, Y27, Y28, Y29, ID_1_2, ID_2_2,
                      Y30, Y31, Y32, Y33, Y34, Y35, Y36, Y37, Y38, Y39, Y40,
                      Y41, Y42, Y43, Y44, Y45, Y46, Y47, Y48, Y49, Y50, Y51,
                      Y52, Y53, Y54, Y55, Y56, Y57, Y58)
                 ) + '\n'
             )


def read_target(path):
    with open(path) as file:
        for line in file:
            record = line.rstrip('\n').split('\t')
            size = len(record)
            if size == 4:
                month, id11, id12, churn = record
                churn = bool(float(churn))
            else:
                assert size == 3
                month, id11, id12 = record
                churn = None
            month = datetime.strptime(month, '%Y-%m')
            id = id11 + id12
            yield TargetRecord(month, id, churn)


def write_target(data, path):
    with open(path, 'w') as file:
        for record in data:
            id11, id12 = split_id(record.id)
            file.write(
                '{month}\t{id11}\t{id12}\t{churn}\n'.format(
                    month=datetime.strftime(record.month, '%Y-%m'),
                    id11=id11,
                    id12=id12,
                    churn=float(record.churn)
                )
            )


def filter_train_by_ids(train, ids):
    for record in train:
        if record.id1 in ids or record.id2 in ids:
            yield record


def count_ids_records(train, ids):
    both_ids_in = 0
    histogram = Counter()
    for record in train:
        id1 = record.id1
        id2 = record.id2
        use_id1 = id1 in ids
        use_id2 = id2 in ids
        if use_id1 and use_id2:
            both_ids_in += 1
        if use_id1:
            histogram[id1] += 1
        elif use_id2:
            histogram[id2] += 1
    return both_ids_in, histogram


def get_churn_histories(target):
    histories = defaultdict(lambda: defaultdict(lambda: None))
    for record in target:
        histories[record.id][record.month] = record.churn
    return histories


def mine_churn_history_rules(histories):
    rules = defaultdict(Counter)
    for id, churns in histories.iteritems():
        rules[churns[NOVEMBER], churns[DECEMBER]][churns[JANUARY]] += 1
    return rules


def get_previous_month(date):
    year = date.year
    month = date.month
    month -= 1
    if month < 1:
        year -= 1
        month = 12
    return datetime(year, month, date.day)


def filter_target_by_month(target, month):
    for record in target:
        if record.month == month:
            yield record


def filter_target_by_ids(target, ids):
    for record in target:
        if record.id in ids:
            yield record


def predict_churn0(record):
    if random.random() < 0.5:
        return False
    return True


def predict_churns0(target):
    for record in target:
        guess = predict_churn0(record)
        yield record._replace(churn=guess)


def predict_churn1(record, churn_histories, churn_history_rules):
    id = record.id
    if id not in churn_histories:
        # Since False is much more common
        return False
    else:
        churns = churn_histories[id]
        previous_month = get_previous_month(record.month)
        before_previous_month = get_previous_month(previous_month)
        guesses = churn_history_rules[churns[before_previous_month],
                                      churns[previous_month]]
        if len(guesses) == 1:
            top, = list(guesses)
            if top is None:
                return False
            else:
                return top
        else:
            (top, count), (second_top, count) = guesses.most_common(2)
            if top is None:
                return second_top
            else:
                return top


def predict_churns1(target, churn_histories, churn_history_rules):
    for record in target:
        guess = predict_churn1(record, churn_histories, churn_history_rules)
        yield record._replace(churn=guess)

            
def get_errors(guesses, etalon):
    churns = {_.id: _.churn for _ in etalon}
    for record in guesses:
        if record.churn != churns[record.id]:
            yield record


def get_roc(guesses, etalon):
    churns = {_.id: _.churn for _ in etalon}
    y_true = []
    y_score = []
    for record in guesses:
        correct = churns[record.id]
        y_true.append(float(correct))
        guess = record.churn
        y_score.append(float(guess))
    fpr, tpr, _ = roc_curve(y_true, y_score)
    return fpr, tpr


def show_roc(roc):
    fpr, tpr = roc
    auc = get_auc(roc)
    plt.plot(fpr, tpr, label='ROC curve (area = %0.2f)' % auc)
    plt.plot([0, 1], [0, 1], '--', c='000', alpha=0.3)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    plt.legend(loc='lower right')


def get_auc(roc):
    fpr, tpr = roc
    return auc(fpr, tpr)


def get_id_pairs_histogram(train):
    histogram = Counter()
    for record in train:
        histogram[record.id1, record.id2] += 1
    return histogram


def get_in_out_transactions(pairs_histogram):
    in_transactions = Counter()
    out_transactions = Counter()
    for (source, target), count in pairs_histogram.iteritems():
        in_transactions[target] += count
        out_transactions[source] += count
    return in_transactions, out_transactions


def build_transactions_graph(edges, **groups):
    nodes = Counter()
    for (source, target), count in edges.iteritems():
        nodes[source] += 1
    graph = nx.DiGraph()
    for id, weight in nodes.iteritems():
        attributes = {'weight': weight}
        for attribute, group in groups.iteritems():
            if id in group:
                attributes[attribute] = group[id]
        graph.add_node(
            id,
            **attributes
        )
    for (source, target), weight in edges.iteritems():
        weight = float(weight) / nodes[source]
        graph.add_edge(
            source,
            target,
            weight=weight
        )
    return graph


def write_transactions_graph(graph, path=TRANSACTIONS_GRAPH):
    nx.write_gexf(graph, path)


def predict_churn2(record,
                   churn_histories,
                   churn_history_rules,
                   in_transactions,
                   out_transactions):
    guess = predict_churn1(record, churn_histories, churn_history_rules)
    if guess is True:
        return guess
    else:
        id = record.id
        if (id in in_transactions and in_transactions[id] <= 10
            or id in out_transactions and 5 <= out_transactions[id] <= 10):
            return True
        else:
            return False


def predict_churns2(target,
                    churn_histories,
                    churn_history_rules,
                    in_transactions,
                    out_transactions):
    for record in target:
        guess = predict_churn2(
            record,
            churn_histories,
            churn_history_rules,
            in_transactions,
            out_transactions
        )
        yield record._replace(churn=guess)


def order_train_by_id1(train):
    return sorted(train, key=lambda _: _.id1)


def maybe_timestamp(value):
    try:
        return datetime.strptime(value, '%d.%m.%Y %H:%M')
    except (ValueError, TypeError):
        return None


def parse_null(value, none='null'):
    if value == none:
        return None
    else:
        return value


def maybe_int(value):
    try:
        return int(value)
    except (ValueError, TypeError):
        return None


def maybe_float(value):
    try:
        return float(value)
    except (ValueError, TypeError):
        return None


def make_train_table(train):
    data = []
    for record in train:
        features = record.features
        data.append((
            record.id1,
            record.id2,
            maybe_timestamp(parse_null(features[0])),
            maybe_timestamp(parse_null(features[1])),
            maybe_timestamp(parse_null(features[2])),
            maybe_int(parse_null(features[5])),
            parse_null(features[6]),
            parse_null(features[7]),
            maybe_int(parse_null(features[24])),
            parse_null(features[25]),
            parse_null(features[26]),
            parse_null(features[43]),
            maybe_float(parse_null(features[48])),
            maybe_float(parse_null(features[49])),
            maybe_float(parse_null(features[50])),
            features[52],
            features[54],
            features[56],
        ))
    return pd.DataFrame(
        data,
        columns=['id1', 'id2', 'date1', 'date2', 'date3',
                 'index1', 'address1', 'address_type1',
                 'index2', 'address2', 'address_type2',
                 'type',
                 'amount1', 'amount2', 'amount3',
                 'currency1', 'currency2',
                 'action'
        ]
    )


def get_month(date):
    return datetime(date.year, date.month, 1)


def get_in_out_transactions_by_months(train, feature=0):
    in_transactions = defaultdict(Counter)
    out_transactions = defaultdict(Counter)
    for record in train:
        date = maybe_timestamp(parse_null(record.features[feature]))
        if date:
            month = get_month(date)
            out_transactions[record.id1][month] += 1
            in_transactions[record.id2][month] += 1
    return in_transactions, out_transactions


def show_transactions_by_months(months_transactions, min_year=2000):
    histogram = Counter()
    for counts in months_transactions.itervalues():
        for month, count in counts.iteritems():
            if month.year >= min_year:
                histogram[month] += count
    table = pd.Series(histogram)
    table[-20:].plot()


def show_ids_transactions_by_months(months_transactions, ids,
                                    min_year=2014, alpha=0.01):
    histogram = Counter()
    for id in ids:
        for month, count in months_transactions[id].iteritems():
            if month.year >= min_year:
                histogram[id, month] = count
    table = pd.Series(histogram)
    table = table.unstack()
    table = table.T
    table = table.fillna(0)
    table = table.div(table.sum(axis=0), axis=1)
    table.plot(cmap='gray', alpha=alpha, legend=False)


def show_ids_transactions_change(months_transactions, ids,
                                 year=2014, alpha=0.01):
    histogram = Counter()
    for id in ids:
        months = months_transactions[id]
        if months:
            start = min(months)
            if start.year == year:
                for month, count in months.iteritems():
                    if month.year == year:
                        histogram[id, month.month - start.month] = count
    table = pd.Series(histogram)
    table = table.unstack()
    table = table.T
    table = table.fillna(0)
    table = table.div(table.sum(axis=0), axis=1)
    table.plot(cmap='gray', alpha=alpha, legend=False)


def show_months_count_distribution(months_transactions, ids, top=100):
    histogram = Counter()
    for id in ids:
        months = months_transactions[id]
        histogram[len(months)] += 1
    pd.Series(histogram)[:12].plot(kind='bar')


def predict_churn3(record,
                   churn_histories,
                   churn_history_rules,
                   in_transactions,
                   out_transactions,
                   in_months_transactions,
                   out_months_transactions):
    guess = predict_churn2(
        record,
        churn_histories,
        churn_history_rules,
        in_transactions,
        out_transactions
    )
    if guess is False:
        return False
    else:
        months = in_months_transactions[record.id]
        if len(months) > 2:
            return False
        else:
            return True


def predict_churns3(target,
                    churn_histories,
                    churn_history_rules,
                    in_transactions,
                    out_transactions,
                    in_months_transactions,
                    out_months_transactions):
    for record in target:
        guess = predict_churn3(
            record,
            churn_histories,
            churn_history_rules,
            in_transactions,
            out_transactions,
            in_months_transactions,
            out_months_transactions
        )
        yield record._replace(churn=guess)


def distance_between_months(left, right):
    assert right >= left
    left_year = left.year
    right_year = right.year
    if left_year == right_year:
        return right.month - left.month
    else:
        return (right.month - left.month) % 12 + (right.year - left.year - 1) * 12


def make_id_features(record,
                     churn_histories,
                     in_months_transactions, out_months_transactions,
                     ids_features_histograms,
                     in_amounts, out_amounts):
    id = record.id
    month = record.month
    if id not in churn_histories:
        previous_month_churn = UNKNOWN_CHURN
        before_previous_month_churn = UNKNOWN_CHURN
    else:
        churns = churn_histories[id]
        previous_month = get_previous_month(month)
        previous_month_churn = churns[previous_month]
        if previous_month_churn is None:
            previous_month_churn = UNKNOWN_CHURN
        else:
            previous_month_churn = CHURN if previous_month_churn else STAY
        before_previous_month = get_previous_month(previous_month)
        before_previous_month_churn = churns[before_previous_month]
        if before_previous_month_churn is None:
            before_previous_month_churn = UNKNOWN_CHURN
        else:
            before_previous_month_churn = CHURNS if before_previous_month_churn else STAY
    months_transactions = in_months_transactions[id]
    in_transactions = sum(months_transactions.values())
    # NOTE It is strange to have len(months) = 0. It happens because
    # of None timestamps. There are 1-2% of such records so ignore
    # them for now
    in_months = len(months_transactions)
    if in_months > 0:
        first_month = min(months_transactions)
        last_month = max(months_transactions)
        months_to_first_in_transaction = distance_between_months(first_month, month)
        months_to_last_in_transaction = distance_between_months(last_month, month)
    else:
        months_to_first_in_transaction = 0
        months_to_last_in_transaction = 0
    months_transactions = out_months_transactions[id]
    out_transactions = sum(months_transactions.values())
    out_months = len(months_transactions)
    if out_months > 0:
        first_month = min(months_transactions)
        last_month = max(months_transactions)
        months_to_first_out_transaction = distance_between_months(first_month, month)
        months_to_last_out_transaction = distance_between_months(last_month, month)
    else:
        months_to_first_out_transaction = 0
        months_to_last_out_transaction = 0
    
    features_histogram = ids_features_histograms[id]
    address_types = features_histogram[7]
    if address_types:
        (address_type, _), = address_types.most_common(1)
        address_type = ADDRESS_TYPES_MAPPING[address_type]
    else:
        address_type = UNKOWN_ADDRESS_TYPE

    feature_8_histogram = features_histogram[8]
    if feature_8_histogram:
        (feature_8, _), = feature_8_histogram.most_common(1)
        feature_8 = FEATURE_8_MAPPING[feature_8]
    else:
        feature_8 = UNKOWN_FEATURE_8

    feature_9_histogram = features_histogram[9]
    if feature_9_histogram:
        (feature_9, _), = feature_9_histogram.most_common(1)
        feature_9 = FEATURE_9_MAPPING[feature_9]
    else:
        feature_9 = UNKOWN_FEATURE_9

    regions_histogram = features_histogram[6]
    if regions_histogram:
        (region, _), = regions_histogram.most_common(1)
        region = region.decode('utf8').lower()
        region = REGIONS_MAPPING[region]
    else:
        region = UNKOWN_REGION

    currencies_histogram = features_histogram[52]
    if currencies_histogram:
        (currency, _), = currencies_histogram.most_common(1)
        currency = CURRENCIES_MAPPING[currency]
    else:
        currency = UNKNOWN_CURRENCY

    feature_18_histogram = features_histogram[18]
    counts = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for value, count in feature_18_histogram.iteritems():
        counts[FEATURE_18_MAPPING[value]] += count
    total = sum(counts)
    if total > 0:
        counts = [float(_) / total for _ in counts]
    (feature_18_1, feature_18_2, feature_18_3, feature_18_4, feature_18_5,
     feature_18_6, feature_18_7, feature_18_8, feature_18_9, feature_18_10) = counts

    feature_19_histogram = features_histogram[19]
    counts = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for value, count in feature_19_histogram.iteritems():
        counts[FEATURE_19_MAPPING[value]] += count
    total = sum(counts)
    if total > 0:
        counts = [float(_) / total for _ in counts]
    (feature_19_1, feature_19_2, feature_19_3, feature_19_4, feature_19_5,
     feature_19_6, feature_19_7, feature_19_8, feature_19_9, feature_19_10) = counts

    feature_21_histogram = features_histogram[21]
    counts = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for value, count in feature_21_histogram.iteritems():
        counts[FEATURE_21_MAPPING[value]] += count
    total = sum(counts)
    if total > 0:
        counts = [float(_) / total for _ in counts]
    (feature_21_1, feature_21_2, feature_21_3, feature_21_4, feature_21_5,
     feature_21_6, feature_21_7, feature_21_8, feature_21_9, feature_21_10) = counts

    feature_22_histogram = features_histogram[22]
    counts = [0, 0, 0, 0, 0]
    for value, count in feature_22_histogram.iteritems():
        counts[FEATURE_22_MAPPING[value]] += count
    total = sum(counts)
    if total > 0:
        counts = [float(_) / total for _ in counts]
    feature_22_1, feature_22_2, feature_22_3, feature_22_4, feature_22_5 = counts

    feature_43_histogram = features_histogram[43]
    counts = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for value, count in feature_43_histogram.iteritems():
        counts[FEATURE_43_MAPPING[value]] += count
    total = sum(counts)
    if total > 0:
        counts = [float(_) / total for _ in counts]
    (feature_43_1, feature_43_2, feature_43_3, feature_43_4, feature_43_5,
     feature_43_6, feature_43_7, feature_43_8, feature_43_9, feature_43_10) = counts

    feature_45_histogram = features_histogram[45]
    counts = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for value, count in feature_45_histogram.iteritems():
        counts[FEATURE_45_MAPPING[value]] += count
    total = sum(counts)
    if total > 0:
        counts = [float(_) / total for _ in counts]
    (feature_45_1, feature_45_2, feature_45_3, feature_45_4, feature_45_5,
     feature_45_6, feature_45_7, feature_45_8, feature_45_9, feature_45_10) = counts

    feature_47_histogram = features_histogram[47]
    counts = [0, 0, 0, 0, 0]
    for value, count in feature_47_histogram.iteritems():
        counts[FEATURE_47_MAPPING[value]] += count
    total = sum(counts)
    if total > 0:
        counts = [float(_) / total for _ in counts]
    feature_47_1, feature_47_2, feature_47_3, feature_47_4, feature_47_5 = counts

    types_histogram = features_histogram[56]
    counts = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    for value, count in types_histogram.iteritems():
        counts[TYPES_MAPPING[value]] += count
    total = sum(counts)
    if total > 0:
        counts = [float(_) / total for _ in counts]
    (types_1, types_2, types_3, types_4, types_5,
     types_6, types_7, types_8, types_9, types_10) = counts

    in_amount = in_amounts[id]
    out_amount = out_amounts[id]

    return Features(
        before_previous_month_churn,
        previous_month_churn,
        in_transactions,
        out_transactions,
        in_months,
        out_months,
        months_to_first_in_transaction,
        months_to_last_in_transaction,
        months_to_first_out_transaction,
        months_to_last_out_transaction,
        address_type,
        feature_8,
        feature_9,
        region,
        currency,

        feature_18_1, feature_18_2, feature_18_3, feature_18_4, feature_18_5,
        feature_18_6, feature_18_7, feature_18_8, feature_18_9, feature_18_10,

        feature_19_1, feature_19_2, feature_19_3, feature_19_4, feature_19_5,
        feature_19_6, feature_19_7, feature_19_8, feature_19_9, feature_19_10,

        feature_21_1, feature_21_2, feature_21_3, feature_21_4, feature_21_5,
        feature_21_6, feature_21_7, feature_21_8, feature_21_9, feature_21_10,

        feature_22_1, feature_22_2, feature_22_3, feature_22_4, feature_22_5,

        feature_43_1, feature_43_2, feature_43_3, feature_43_4, feature_43_5,
        feature_43_6, feature_43_7, feature_43_8, feature_43_9, feature_43_10,

        feature_45_1, feature_45_2, feature_45_3, feature_45_4, feature_45_5,
        feature_45_6, feature_45_7, feature_45_8, feature_45_9, feature_45_10,

        feature_47_1, feature_47_2, feature_47_3, feature_47_4, feature_47_5,

        types_1, types_2, types_3, types_4, types_5, types_6,
        types_7, types_8, types_9, types_10,

        in_amount, out_amount
    )


def make_features(target,
                  churn_histories,
                  in_months_transactions, out_months_transactions,
                  ids_features_histograms,
                  in_amounts, out_amounts):
    for record in target:
        yield make_id_features(
            record,
            churn_histories,
            in_months_transactions, out_months_transactions,
            ids_features_histograms,
            in_amounts, out_amounts
        )


def split_in_train_test(features, target, train_share=0.5):
    train_features = []
    test_features = []
    train_target = []
    test_target = []
    for features_record, target_record in zip(features, target):
        if random.random() < train_share:
            train_features.append(features_record)
            train_target.append(target_record)
        else:
            test_features.append(features_record)
            test_target.append(target_record)
    return train_features, test_features, train_target, test_target


def get_train_test_indexes(size, train_share=0.9):
    indexes = range(size)
    train_indexes = []
    test_indexes = []
    for index in indexes:
        if random.random() < train_share:
            train_indexes.append(index)
        else:
            test_indexes.append(index)
    return train_indexes, test_indexes


def get_train_test_indexes_slice(size, slice, slices):
    indexes = range(size)
    slice_size = size / slices
    train_indexes = indexes[:slice * slice_size] + indexes[(slice + 1) * slice_size:]
    test_indexes = indexes[slice * slice_size:(slice + 1) * slice_size]
    return train_indexes, test_indexes


def get_indexes(sequence, indexes):
    if isinstance(sequence, (tuple, list)):
        return [sequence[index] for index in indexes]
    elif isinstance(sequence, np.ndarray):
        return sequence[indexes]


def get_feature_matrix(features):
    names = []
    chunks = []
    table = pd.DataFrame(features, columns=FEATURES_NAMES)
    for name in FEATURES_NAMES:
        chunk = table[[name]].as_matrix()
        if name in CATEGORICAL_FEATURES:
            encoder = OneHotEncoder(
                n_values=CATEGORICAL_FEATURES[name],
                dtype=np.float32,
                sparse=False
            )
            chunk = encoder.fit_transform(chunk)
        chunks.append(chunk)
        _, columns = chunk.shape
        if columns == 1:
            names.append(name)
        else:
            for index in range(columns):
                names.append(
                    '{name}_{index}'
                    .format(name=name, index=index + 1)
                )
    matrix = np.hstack(chunks)
    return names, matrix


def get_target_matrix(target):
    return [int(_.churn) for _ in target]


def train_model(X, y, estimators=(10, 500), depth=(2, 10), jobs=1, folds=3):
    scaler = StandardScaler()
    forest = RandomForestClassifier()
    estimator = Pipeline([('scaler', scaler), ('forest', forest)])

    param_grid = {
        'forest__n_estimators': estimators,
        'forest__max_depth': depth,
    }
    model = GridSearchCV(
        estimator=estimator,
        param_grid=param_grid,
        n_jobs=jobs,
        scoring='roc_auc',
        verbose=10,
        cv=folds
    )
    model.fit(X, y)
    return model


def train_forest_model(X, y, estimators=250, depth=10):
    scaler = StandardScaler()
    forest = RandomForestClassifier(
        n_estimators=estimators,
        max_depth=depth
    )
    model = Pipeline([('scaler', scaler), ('forest', forest)])
    model.fit(X, y)
    return model


def describe_model(model, feature_names):
    print 'Best score: {0:0.3f}'.format(model.best_score_)
    estimator = model.best_estimator_
    print 'Parameters:'
    parameters = estimator.get_params()
    for name in ['forest__n_estimators', 'forest__max_depth']:
        print '\t{0}: {1}'.format(name, parameters[name])
    pipeline = dict(model.best_estimator_.steps)
    forest = pipeline['forest']
    importance = dict(zip(feature_names, forest.feature_importances_))
    print len(importance)
    print 'Importance:'
    for name in sorted(importance, key=importance.get, reverse=True):
        print '\t{0} {1}'.format(importance[name], name)


def predict_churns4(target, model, features):
    if isinstance(model, GridSearchCV):
        estimator = model.best_estimator_
    else:
        estimator = model
    guesses = estimator.predict_proba(features)[:, 1]
    for (record, guess) in zip(target, guesses):
        yield record._replace(churn=guess)


def threshold_guesses(guesses, threshold=0.5):
    for record in guesses:
        yield record._replace(churn=(record.churn > threshold))


def search_for_threshold(guesses, etalon, start=0, stop=1, step=0.01):
    measures = {}
    for threshold in np.arange(start, stop, step):
        thresholded = threshold_guesses(guesses, threshold)
        roc = get_roc(thresholded, etalon)
        auc = get_auc(roc)
        measures[threshold] = auc
    return max(measures, key=measures.get)


def count_features_distinct_values(train, features=range(58)):
    from HLL import HyperLogLog

    # NOTE 2^10 registers seems to correspond to ~1-2% error
    estimators = {_: HyperLogLog(10) for _ in features}
    for record in train:
        values = record.features
        for feature in features:
            estimator = estimators[feature]
            value = values[feature]
            estimator.add(value)
    return {feature: estimator.cardinality()
            for feature, estimator in estimators.iteritems()}
    

def count_features_histograms(train, features=[1, 5, 6, 7, 8, 9, 11,
                                               12, 14, 15, 18, 19, 20, 21, 22, 24, 25,
                                               26, 27, 28, 30, 31, 33, 34, 37, 38, 39,
                                               40, 41, 42, 43, 44, 45, 46, 47, 51, 52,
                                               53, 54, 56, 57]):
    histograms = {_: Counter() for _ in features}
    for record in train:
        values = record.features
        for feature in features:
            value = values[feature]
            histograms[feature][value] += 1
    return histograms


def show_features_histograms(histograms, top=10):
    for feature in sorted(histograms,
                          key=lambda feature: len(histograms[feature])):
        histogram = histograms[feature]
        print feature, len(histogram)
        total = sum(histogram.values())
        for value, count in histogram.most_common(top):
            print '\t{0:0.3f}\t{1}'.format(float(count) / total, value)


def count_ids_features_histograms(train, ids,
                                  features_pairs=[(5, 24), (6, 25), (7, 26),
                                                  (8, 27), (9, 28), (11, 30),
                                                  (12, 31), (14, 33), (15, 34),
                                                  (18, 37), (19, 38), (21, 40),
                                                  (22, 41), (52, 54)],
                                  single_features=[43, 45, 47, 56, 57]):
    histograms = defaultdict(lambda: defaultdict(Counter))
    for record in train:
        id1 = record.id1
        id2 = record.id2
        features = record.features
        for id1_feature, id2_feature in features_pairs:
            if id1 in ids:
                value = features[id1_feature]
                histograms[id1][id1_feature][value] += 1
            if id2 in ids:
                value = features[id2_feature]
                histograms[id2][id1_feature][value] += 1
        for feature in single_features:
            if id1 in ids:
                value = features[feature]
                histograms[id1][feature][value] += 1
    return histograms


def make_features_ids_histograms(ids_features_histograms):
    histograms = defaultdict(Counter)
    for id, features in ids_features_histograms.iteritems():
        for feature, values in features.iteritems():
            histograms[feature][len(values)] += 1
    return histograms


def show_features_ids_histograms(histograms, top=10):
    data = []
    for feature, histogram in histograms.iteritems():
        for ids, count in histogram.iteritems():
            data.append((feature, ids, count))
    table = pd.DataFrame(data, columns=['feature', 'ids', 'count'])
    table = table.pivot('ids', 'feature', 'count')
    table[:top].plot(
        kind='bar', cmap='gray',
        subplots=True, layout=(4, -1),
        legend=False, figsize=(10, 10)
    )


def make_features_values_histograms(ids_features_histograms):
    histograms = defaultdict(Counter)
    for id, features in ids_features_histograms.iteritems():
        for feature, values in features.iteritems():
            for value in values:
                histograms[feature][value] += 1
    return histograms


def show_features_values_histograms(histograms, top=10):
    for feature, values in sorted(histograms.iteritems()):
        print feature, 'unique:', len(values)
        for value, ids in values.most_common(top):
            print '\t{0}\t{1}'.format(ids, value)


def show_grid_scores(model):
    data = []
    for record in model.grid_scores_:
        parameters = record.parameters
        trees = parameters['forest__n_estimators']
        depth = parameters['forest__max_depth']
        scores = record.cv_validation_scores
        data.append((trees, depth, scores.mean(), scores.std()))
    table = pd.DataFrame(data, columns=['trees', 'depth', 'average', 'error'])
    table['lower_bound'] = table.average - table.error
    table = table.pivot('trees', 'depth', 'lower_bound')
    sns.heatmap(table)


def get_ids_in_out_amounts(train, ids, feature=48):
    in_amounts = Counter()
    out_amounts = Counter()
    for record in train:
        amount = parse_null(record.features[feature])
        if amount is not None:
            amount = float(amount)
            id1 = record.id1
            if id1 in ids:
                out_amounts[id1] += amount
            id2 = record.id2
            if id2 in ids:
                in_amounts[id2] += amount
    return in_amounts, out_amounts
